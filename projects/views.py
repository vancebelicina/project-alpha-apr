from django.shortcuts import get_object_or_404, redirect, render
from projects.forms import ProjectForm
from projects.models import Project
from django.contrib.auth.decorators import login_required

# Create your views here.


@login_required
def project_list(request):
    project = Project.objects.all().filter(owner=request.user)
    context = {
        "project_list": project,
    }

    return render(request, "projects/project_list.html", context)


@login_required
def show_project(request, id):
    project = get_object_or_404(Project, id=id)
    context = {
        "project": project,
    }

    return render(request, "projects/project_detail.html", context)


@login_required
def create_project(request):
    if request.method == "POST":
        form = ProjectForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect("list_projects")
    else:
        form = ProjectForm()

    context = {
        "form": form,
    }

    return render(request, "projects/create_project.html", context)
